import data_catalog
import config


# input
para            = config.dc_parameters().one_schema_para
csv_file        = config.dc_parameters().raw_data
dc              = data_catalog.data_catalog_engin(csv_file)
schema_name     = para['schema_name']
txt_name        = para['txt_name']
keys            = para['keys']


# output
dc.oneSchema_to_md(dc.raw_df, schema_name, txt_name, keys)
