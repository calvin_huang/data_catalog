import pandas as pd
import os
from itertools import combinations
import numpy as np


class data_catalog_engin:
    def __init__(self, csv_file):
        self.csv_file = csv_file
        self.cwd = os.getcwd() + '/' + self.csv_file
        self.raw_csv = pd.read_csv(self.cwd)
        self.raw_df = self.concatDataType()

    def concatDataType(self):
        df = self.raw_csv
        data_type = df['data_type'].values.tolist()
        length = df['character_maximum_length'].values.tolist()
        type_list = []
        for i, j in zip(data_type, length):
            if np.isnan(j):
                type_list.append(i)
            else:
                type_list.append(i+' ('+str(j)+')')
        df = df.drop(columns=['data_type', 'character_maximum_length'])
        df['data_type'] = type_list
        return df

    def table_to_md(self, txt_name, table_df, table_name=None, purpose=None, status=None, comp_df=None, link=None, para='a'):
        # para = 'a' - Append - will append to the end of the file
        # para = 'w' - Write - will overwrite any existing content
        """
        Table_name
        Purpose:
        Status:
        |    | column_name            | data_type   | Definition   | Related key(s)   | Remarks   |
        |---:|:-----------------------|:------------|:-------------|:-----------------|:----------|
        |  0 | date                   | date        |              |                  |           |
        |  1 | hour                   | bigint      |              |                  |           |
        |  2 | minute                 | bigint      |              |                  |           |
        [Return to top]
        """
        with open(txt_name, para) as f:
            if table_name is not None:
                f.write('\n\n## ' + table_name)
            if purpose is not None:
                f.write('\n\nPurpose: ' + purpose)
            if status is not None:
                f.write('\n\nStatus: ' + status)
            if comp_df is not None:
                f.write('\n\n')
                f.write(comp_df.to_markdown())
            f.write('\n\n')
            f.write(table_df.to_markdown(index=False))
            if link:
                f.write('\n\n[Return to top](' + link + '#top)\n')

    def df_to_md(self, txt_name, table_name, info, df, link=None, para='a'):
        # para = 'a' - Append - will append to the end of the file
        # para = 'w' - Write - will overwrite any existing content
        """
        ## Table name
        info
        |                                  | neuron_australia   | neuron_canada   | neuron_gb   | neuron_korea   |
        |:---------------------------------|:-------------------|:----------------|:------------|:---------------|
        | adl_cbd_status_count             | 1                  |                 |             |                |
        | adl_wa_status_count              | 1                  |                 |             |                |
        link
        """
        f = open(txt_name, para)
        f.write('\n\n# ' + table_name)
        if info:
            f.write('\n\n' + info + '\n\n')
        else:
            f.write('\n\n')
        f.write(df.to_markdown())
        if link:
            f.write('\n\n[Return to top](' + link + '#top)\n')
        f.write('\n\n')
        f.close()
        print('{} added to \'{}\''.format(table_name, txt_name))

    def oneSchema_to_md(self, df, schema_name, txt_name, keys=None, removed_table=None):
        table_names, table_df_dic = self.getTablesOfASchema(df=df, schema_name=schema_name, keys=keys, removed_table=None)
        with open(txt_name, 'a') as f:
            f.write('\n\n# ' + schema_name)
            f.write('\n\n')
        for table in table_names:
            table_df = table_df_dic[table]
            self.table_to_md(txt_name=txt_name, table_df=table_df, table_name=table)

    def multiSchemas_to_md(self, df, schema_names, txt_name, keys_dict=None, removed_table=None):
        for schema_name in schema_names:
            if isinstance(keys_dict, dict):
                keys = keys_dict[schema_name]
            elif isinstance(keys_dict, list):
                keys = keys_dict
            else:
                keys = None
            self.oneSchema_to_md(df, schema_name, txt_name, keys, removed_table=None)

    def combinedSchemas_to_md(self, txt_name, df, schemas_to_combine, link=None, para='a', keys=None, 
                              removed_table=None, add_comp_table=True):
        # add comparison table
        if add_comp_table:
            comp_df = self.getComparisonTable(df, schemas_to_combine)
            self.df_to_md(txt_name=txt_name, table_name='Schema Comparison Table', info=None, df=comp_df, link=None, para='a')

        # add tables
        tables_to_add = []
        all_table_dict = {}
        for schema_name in schemas_to_combine:
            table_names, table_df_dic = self.getTablesOfASchema(df=df, schema_name=schema_name, keys=keys, removed_table=removed_table)
            all_table_dict.update(table_df_dic)
            for table in table_names:
                if table not in tables_to_add:
                    tables_to_add.append(table)
        tables_to_add.sort()
        for table in tables_to_add:
            self.table_to_md(txt_name=txt_name, table_df=all_table_dict[table],
                             table_name=table, purpose=None, status=None, link=link, para='a')
        print('{} tables added to \'{}\''.format(len(tables_to_add), txt_name))

    def getTablesOfASchema(self, df, schema_name, keys=None, remarks=None, removed_table=None):
        # get all tables of ONE given schema
        # df = all schemas
        """
        output:
            table_names = Array(table_name)
            table_df_dic = {table_name: col_name_df}
        """
        if removed_table is not None:
            temp = df.loc[df['table_schema'] == schema_name]
            schema_df = temp.loc[~temp['table_name'].isin(removed_table)].reset_index(drop=True)
        else:
            schema_df = df.loc[df['table_schema'] == schema_name].reset_index(drop=True)
        table_df = schema_df.drop('table_schema', axis=1)

        # add related keys
        if keys is not None:
            column_names = table_df['column_name'].values
            related_keys = []
            for name in column_names:
                if name in keys:
                    related_keys.append('.'.join(name.split('_')))
                else:
                    related_keys.append(' ')
            table_df['related_keys'] = related_keys
        else:
            table_df['related_keys'] = ' '

        if remarks == None:
            table_df['remarks'] = ' '

        table_names = table_df.table_name.unique()
        table_names.sort()
        table_df_dic = {}  # table_name: table
        for i in table_names:
            table_df_dic[i] = table_df.loc[table_df['table_name'] == i].reset_index(drop=True).drop('table_name',
                                                                                                    axis=1).fillna('')
        return table_names, table_df_dic

    def getTableNamesFromSchema(self, df, schema_name):
        """
        input : schema_name
        output: Array(table_names)
        """
        schema_df = df.loc[df['table_schema'] == schema_name].reset_index(drop=True)
        return schema_df.table_name.unique()

    def getColNamesFromSchemaTable(self, df, schema_name, table_name):
        """
        input : schema_name, table_name
        output: Array(column_names)
        """
        table_df = df.loc[(df['table_schema'] == schema_name) & (df['table_name'] == table_name)].reset_index(drop=True)
        return table_df.column_name.unique()

    def similarityRateBySchemaNames(self, df, schema_names, num, output_name=None, include_col=False):
        """
        schema_names:  [Array] schema_name
        num:           Length of the sequence
        output_name:   Name of the output csv file (=None if no need)
        include_col:   [Bool] True if need to get SR_column

        output:        [DataFrame] Similarity Rate result
        """
        schema_combs = list(combinations(schema_names, num))
        groupby_schema = df.groupby('table_schema')
        if include_col:
            groupby_schemaTable = df.groupby(['table_schema', 'table_name'])
        schema_table_dict = {}
        for schema_name in schema_names:
            temp = groupby_schema.get_group(schema_name).table_name.unique()
            schema_table_dict[schema_name] = temp

        columns = []
        for i in range(num):
            columns.append('Schema_{}'.format(i + 1))
        for i in range(num):
            columns.append('Percen_{}'.format(i + 1))
        columns.append('Average_tableSR')
        if include_col:
            columns.append('Average_colSR')
        comparison_df = pd.DataFrame(columns=columns)
        for schema_comb in list(schema_combs):
            names, sets, rates = [], [], []
            for i in range(num):
                names.append(schema_comb[i])
                sets.append(set(schema_table_dict[schema_comb[i]]))
            common_tables = sets[0].intersection(*sets[1:])  # use of *
            c = len(common_tables)
            for i in range(num):
                rates.append(c / len(sets[i]))
            average = sum(rates) / len(rates)
            avgColSR = 0
            if include_col:
                sr_col = []  # sr1, sr2, sr3
                for table in common_tables:
                    col_sets = []  # colnames1, colnames2, colnames3
                    for schema in schema_comb:
                        col_names = groupby_schemaTable.get_group((schema, table))['column_name'].unique()
                        col_sets.append(set(col_names))
                    common_cols = col_sets[0].intersection(*col_sets[1:])
                    srList = []
                    for col_names in col_sets:
                        srList.append(len(common_cols) / len(col_names))
                    avgColSR = sum(srList) / len(srList)
                d = dict(zip(columns, names + rates + list([average]) + list([avgColSR])))
            else:
                d = dict(zip(columns, names + rates + list([average])))
            # comparison_df = comparison_df.append(d, ignore_index=True)
            comparison_df = pd.concat([comparison_df, pd.DataFrame.from_dict([d])])
        if output_name != None:
            comparison_df.to_csv(output_name)
        return comparison_df

    def getComparisonTable(self, df, schemas_to_merge):
        all_table_names = []
        each_table_names = []
        for schema in schemas_to_merge:
            table_names, table_df_dic = self.getTablesOfASchema(df, schema)
            each_table_names.append(table_names)
            all_table_names += table_names.tolist()
        all_table_names = list(set(all_table_names))
        all_table_names.sort()

        rows = [[] for i in range(len(all_table_names))]
        for i in range(len(all_table_names)):
            for j in range(len(schemas_to_merge)):
                if all_table_names[i] in each_table_names[j]:
                    rows[i].append('1')
                else:
                    rows[i].append(' ')
        schema_comp_df = pd.DataFrame(np.array(rows), columns=schemas_to_merge, index=all_table_names)
        return schema_comp_df

def getCommonZeroTables(csv_file):
    df = pd.read_csv(csv_file)
    cols = df.columns
    sets = []
    for col in cols:
        sets.append(set(df[col]))
    commons = sets[0].intersection(*sets[1:])
    return list(commons)

