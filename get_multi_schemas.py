import data_catalog
import config


# input
para = config.dc_parameters().multi_schemas_para
csv_file        = config.dc_parameters().raw_data
dc = data_catalog.data_catalog_engin(csv_file)
schema_names    = para['schema_names']
txt_name        = para['txt_name']
keys_dict       = para['keys_dict']


# output
dc.multiSchemas_to_md(dc.raw_df, schema_names, txt_name, keys_dict)

