import pandas as pd
from data_catalog import getCommonZeroTables

class dc_parameters:
    # parameter for similarityRateBySchemaNames()
    raw_data = 'neuron_pro-rds_raw_data.csv'

    sr_para = {
        'schemas': ['neuron_australia', 'neuron_canada', 'neuron_gb', 'neuron_korea', 'neuron_newzealand'],
        'num': 3,
        'output_name': 'result_sr.csv',
        'include_col': False
    }

    one_schema_para = {
        'schema_name': 'neuron_user',
        'txt_name': 'result_one_schema.txt',
        'keys': ['user_id']
    }

    multi_schemas_para = {
        'schema_names': ['neuron_australia', 'neuron_canada'],
        'txt_name': 'result_multi_schemas.txt',
        'keys_dict': {
            'neuron_australia': ['user_id', 'trip_id'],
            'neuron_canada': ['user_id', 'trip_id', 'scooter_id']
        }
    }

    combined_schemas_para = {
        'schemas_to_combine': [
            'neuron_gb',
            'neuron_australia',
            'neuron_newzealand',
            'neuron_canada',
            'neuron_korea',
        ],
        'txt_name': 'result_combined_schemas.md',
        'para': 'a',
        'keys': [
            'user_id',
            'trip_id'
        ],
        'removed_table': getCommonZeroTables('RRD_removedTable.csv'),
        'add_comp_table': False
    }
