# Data Catalog

## Pipeline
1. SQL script

- select table_schema, table_name, column_name, data_type, character_maximum_length 
- from information_schema.columns
- where table_schema != 'pg_catalog' 
- and table_schema != 'information_schema' 
- and table_schema != 'public'

3. export SQL resultSet as csv file
4. edit config
5. run py files