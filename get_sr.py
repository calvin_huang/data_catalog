import data_catalog
import config


# input
para            = config.dc_parameters()
csv_file        = para.raw_data
dc              = data_catalog.data_catalog_engin(csv_file)
# schema_names    = para.sr_para['schemas']
# schema_names    = dc.raw_df['table_schema'].unique().tolist()

num             = para.sr_para['num']
output_name     = para.sr_para['output_name']
include_col     = para.sr_para['include_col']

# output
sr_df = dc.similarityRateBySchemaNames(dc.raw_df, schema_names, num, output_name, include_col)

if output_name is not None:
    print("Output name: ", output_name)
