import data_catalog
import config


# input
parameter           = config.dc_parameters().combined_schemas_para
csv_file            = config.dc_parameters().raw_data
dc                  = data_catalog.data_catalog_engin(csv_file)
schemas_to_combine  = parameter['schemas_to_combine']
txt_name            = parameter['txt_name']
para                = parameter['para']
keys                = parameter['keys']
# keys            = None
removed_table       = parameter['removed_table']
add_comp_table      = parameter['add_comp_table']

# output
dc.combinedSchemas_to_md(txt_name, dc.raw_df, schemas_to_combine, link=None, para=para, keys=keys,
                         removed_table=removed_table, add_comp_table=add_comp_table)
